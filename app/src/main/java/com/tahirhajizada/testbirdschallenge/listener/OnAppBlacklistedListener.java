package com.tahirhajizada.testbirdschallenge.listener;

/**
 * Created by tahir on 4/30/18.
 */

public interface OnAppBlacklistedListener {
    void addAppToBlacklist(String packageName);
    void removeAppFromBlacklist(String packageName);
}

package com.tahirhajizada.testbirdschallenge.model;

import android.graphics.drawable.Drawable;

/**
 * Created by tahir on 4/30/18.
 */

public class AppList {

    private String name;
    private String packageName;
    private boolean blackListState;
    Drawable icon;

    public AppList(String name, String packageName, Drawable icon, boolean blackListState) {
        this.name = name;
        this.packageName = packageName;
        this.icon = icon;
        this.blackListState = blackListState;
    }

    public String getName() {
        return name;
    }

    public Drawable getIcon() {
        return icon;
    }

    public String getPackageName() {
        return packageName;
    }

    public boolean isBlacklisted() {
        return blackListState;
    }

    public void setBlackListState(boolean blackListState) {
        this.blackListState = blackListState;
    }
}

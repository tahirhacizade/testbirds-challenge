package com.tahirhajizada.testbirdschallenge.dao;

import android.content.Context;
import android.content.SharedPreferences;

import com.tahirhajizada.testbirdschallenge.R;

/**
 * Created by tahir on 4/30/18.
 */

public class LocalProperties {
    private SharedPreferences pref;
    private Context context;

    public LocalProperties(Context context){
        this.context = context;
        pref = context.getSharedPreferences(context.getResources().getString(R.string.preference_name), 0);
    }

    public void addData(String data) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(data, true);
        editor.commit();
    }

    public void removeData(String data) {
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(data);
        editor.commit();
    }

    public boolean isExists(String data) {
        return pref.getBoolean(data, false);
    }

    public boolean getDistractionMode() {
        return pref.getBoolean("DistractionMode", false);
    }

    public void setDistractionMode(boolean mode) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("DistractionMode", mode);
        editor.commit();
    }

}

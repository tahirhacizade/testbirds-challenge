package com.tahirhajizada.testbirdschallenge.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.tahirhajizada.testbirdschallenge.listener.OnAppBlacklistedListener;
import com.tahirhajizada.testbirdschallenge.model.AppList;
import com.tahirhajizada.testbirdschallenge.R;

import java.util.List;

/**
 * Created by tahir on 4/30/18.
 */

public class AppListAdapter extends RecyclerView.Adapter<AppListAdapter.ViewHolder> {
    private List<AppList> listOfApps;
    private OnAppBlacklistedListener appBlacklistedListener;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView appName;
        private final TextView appPackageName;
        private final ImageView appIcon;
        private final Switch blackListSwitch;

        public ViewHolder(View v) {
            super(v);

            appName = (TextView) v.findViewById(R.id.app_name);
            appIcon = (ImageView) v.findViewById(R.id.app_icon);
            blackListSwitch = (Switch) v.findViewById(R.id.app_block_switcher);
            appPackageName = (TextView) v.findViewById(R.id.app_package);
            blackListSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int position = getAdapterPosition();
                    String packageName = listOfApps.get(position).getPackageName();

                    listOfApps.get(position).setBlackListState(isChecked);
                    if (isChecked)
                        appBlacklistedListener.addAppToBlacklist(packageName);
                    else
                        appBlacklistedListener.removeAppFromBlacklist(packageName);
                }
            });

        }

        public TextView getAppName() {
            return appName;
        }

        public ImageView getAppIcon() {
            return appIcon;
        }

        public Switch getBlackListSwitch() {
            return blackListSwitch;
        }

        public TextView getAppPackageName() {
            return appPackageName;
        }
    }

    public AppListAdapter(OnAppBlacklistedListener appBlacklistedListener, List<AppList> listOfApps) {
        this.listOfApps = listOfApps;
        this.appBlacklistedListener = appBlacklistedListener;
    }

    @Override
    public AppListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.app_item_view, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.appName.setText(listOfApps.get(position).getName());
        holder.appIcon.setImageDrawable(listOfApps.get(position).getIcon());
        holder.blackListSwitch.setChecked(listOfApps.get(position).isBlacklisted());
        holder.appPackageName.setText(listOfApps.get(position).getPackageName());
    }

    // Return the size of dataset
    @Override
    public int getItemCount() {
        if (listOfApps == null)
            return 0;
        else
            return listOfApps.size();
    }
}

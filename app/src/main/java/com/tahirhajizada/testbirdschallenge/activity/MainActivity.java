package com.tahirhajizada.testbirdschallenge.activity;

import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.tahirhajizada.testbirdschallenge.R;
import com.tahirhajizada.testbirdschallenge.dao.LocalProperties;
import com.tahirhajizada.testbirdschallenge.listener.OnAppBlacklistedListener;
import com.tahirhajizada.testbirdschallenge.model.AppList;
import com.tahirhajizada.testbirdschallenge.adapter.AppListAdapter;
import com.tahirhajizada.testbirdschallenge.service.ForegroundAppService;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnAppBlacklistedListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ToggleButton distractionModeSwitcher;
    private LocalProperties localProperties;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!isAccessGranted()) {
            showAlertDialog();
        }
        // Initialize view elements
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        distractionModeSwitcher = (ToggleButton) findViewById(R.id.switcher);

        localProperties = new LocalProperties(this);

        distractionModeSwitcher.setChecked(localProperties.getDistractionMode());

        // Set RecyclerView
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Get list of installed apps and init an adapter
        List<AppList> installedApps = getInstalledApps();
        mAdapter = new AppListAdapter(this,installedApps);
        mRecyclerView.setAdapter(mAdapter);

        // if Service doesn't run after app memory clean, reinstate it
        if(localProperties.getDistractionMode() == true && isMyServiceRunning(ForegroundAppService.class) == false) {
            Intent intent = new Intent(this,ForegroundAppService.class);
            startService(intent);
        }

    }

    private List<AppList> getInstalledApps() {
        List<AppList> res = new ArrayList<AppList>();
        List<ApplicationInfo> packs = getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);
        for (int i = 0; i < packs.size(); i++) {
            ApplicationInfo app = packs.get(i);
            try {
                if (null != getPackageManager().getLaunchIntentForPackage(app.packageName)) {
                    String appName = app.loadLabel(getPackageManager()).toString();
                    String packageName = app.packageName;
                    Drawable icon = app.loadIcon(getPackageManager());
                    boolean isBlocked = getAppStateFromMemory(packageName);
                    res.add(new AppList(appName, packageName, icon, isBlocked ));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    private boolean isSystemPackage(ApplicationInfo pkgInfo) {
        return ((pkgInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true : false;
    }

    private boolean getAppStateFromMemory(String packageName) {
        return localProperties.isExists(packageName);
    }

    @Override
    public void addAppToBlacklist(String packageName) {
        localProperties.addData(packageName);
    }

    @Override
    public void removeAppFromBlacklist(String packageName) {
        localProperties.removeData(packageName);
    }

    public  void stateChanged(View view){
        Intent intent = new Intent(this,ForegroundAppService.class);

        if(distractionModeSwitcher.isChecked()) {
            localProperties.setDistractionMode(true);
            startService(intent);
        }
        else {
            localProperties.setDistractionMode(false);
            stopService(intent);
        }
    }

    private boolean isAccessGranted() {
        try {
            PackageManager packageManager = getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) getSystemService(Context.APP_OPS_SERVICE);
            int mode = 0;
            if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.KITKAT) {
                mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                        applicationInfo.uid, applicationInfo.packageName);
            }
            return (mode == AppOpsManager.MODE_ALLOWED);

        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void showAlertDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(getResources().getString(R.string.alert_title));
        alertDialog.setMessage(getResources().getString(R.string.alert_message));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.alert_confirm),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
                        startActivity(intent);
                    }
                });
        alertDialog.show();
    }
}

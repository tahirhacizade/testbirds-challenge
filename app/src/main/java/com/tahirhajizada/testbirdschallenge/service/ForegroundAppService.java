package com.tahirhajizada.testbirdschallenge.service;

import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.tahirhajizada.testbirdschallenge.R;
import com.tahirhajizada.testbirdschallenge.activity.MainActivity;
import com.tahirhajizada.testbirdschallenge.dao.LocalProperties;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by tahir on 4/30/18.
 */

public class ForegroundAppService extends Service {

    private Handler handler = new Handler();
    private LocalProperties localProperties;
    private Context context;

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        localProperties = new LocalProperties(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = this;
        handler.postDelayed(runnable, 1000);

        return START_STICKY;
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {

            String currentApp = "";

            UsageStatsManager usm = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
            long time = System.currentTimeMillis();
            List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY,  time - 1000*1000, time);
            if (appList != null && appList.size() > 0) {
                SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
                for (UsageStats usageStats : appList) {
                    mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                }
                if (mySortedMap != null && !mySortedMap.isEmpty()) {
                    currentApp = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
                }
            }

            if(localProperties.isExists(currentApp)) {
                Intent mIntent = new Intent(context, MainActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mIntent);
                Toast.makeText(context, getResources().getString(R.string.toast_text), Toast.LENGTH_LONG).show();
            }
            handler.postDelayed(this, 1000);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
}
